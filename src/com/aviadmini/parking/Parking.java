package com.aviadmini.parking;

import java.util.ArrayList;

public class Parking {

    public static final int NOT_PARKED      = -1;
    private static final int ALREADY_PARKED = -2;

    private ArrayList<Spot> mParkingSpots = new ArrayList<>();

    public Parking(final int pTotalSpots) {

        for (int i = 0; i < pTotalSpots; i++) {
            this.mParkingSpots.add(new Spot(new Car()));
        }

        int increment = 0;
        for (final Spot spot : this.mParkingSpots) {

            spot.setId(increment);

            increment++;

        }

    }

    public void assignFirstSpots(final Car pVehicleClass, final int pNumber) {

        if (pNumber > this.mParkingSpots.size()) {
            throw new IllegalArgumentException("Can't assign more spots than exists");
        }

        for (int i = 0; i < pNumber; i++) {
            this.mParkingSpots.get(i).setMaxAccepted(pVehicleClass);
        }

    }

    public int getSpotsCount() {
        return this.mParkingSpots.size();
    }

    public boolean acceptsVehicle(final Car pVehicle) {

        for (final Spot spot : this.mParkingSpots) {

            if (!spot.hasOccupant() && spot.accepts(pVehicle)) {
                return true;
            }

        }

        return false;
    }

    public int park(final Car pVehicle) {

        if(this.isParked(pVehicle)){
            return ALREADY_PARKED;
        }

        for (int i = 0; i < this.mParkingSpots.size(); i++) {

            final Spot spot = this.mParkingSpots.get(i);

            if (!spot.hasOccupant() && spot.accepts(pVehicle)) {
                spot.setOccupant(pVehicle);
                return i;
            }

        }

        return NOT_PARKED;
    }

    private boolean isParked(final Car pVehicle) {

        for(final Spot spot : this.mParkingSpots){

            if(spot.getOccupant()==pVehicle){
                return true;
            }

        }

        return false;
    }

    public Car unpark(final int pSpotId) {

        if (pSpotId < 0 || pSpotId > this.getSpotsCount()) {
            throw new IllegalArgumentException("Illegal spot id");
        }

        final Spot spot = this.mParkingSpots.get(pSpotId);

        final Car car = spot.hasOccupant() ? spot.getOccupant() : null;
        spot.setOccupant(null);

        return car;
    }

    private class Spot {

        private Car mMaxAccepted;

        private int mId;

        private Car mOccupant = null;

        private Spot(final Car pMaxAccepted) {this.mMaxAccepted = pMaxAccepted;}

        public boolean accepts(final Car pCar) {
            return pCar.getClass().isInstance(mMaxAccepted);
        }

        private void setMaxAccepted(final Car pMaxAccepted) {
            this.mMaxAccepted = pMaxAccepted;
        }

        public Car getMaxAccepted() {
            return this.mMaxAccepted;
        }

        public int getId() {
            return this.mId;
        }

        private void setId(final int pId) {
            this.mId = pId;
        }

        public Car getOccupant() {
            return this.mOccupant;
        }

        public boolean hasOccupant() {
            return this.mOccupant != null;
        }

        public void setOccupant(final Car pOccupant) {
            this.mOccupant = pOccupant;
        }

    }

}
