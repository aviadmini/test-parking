package com.aviadmini.parking;

import java.util.Locale;
import java.util.Random;

public class Main {

    private static final int SLOTS_MAX_DEFAULT = 30;

    public static void main(final String[] pArgs) {

        int spotsTotal = 0;
        int spotsForTrucks = 0;
        if (pArgs != null && pArgs.length >= 2) {

            try {

                spotsTotal = Integer.parseInt(pArgs[0]);
                spotsForTrucks = Integer.parseInt(pArgs[1]);

            } catch (final NumberFormatException e) {
                // ignore because we can set defaults later
            }

        }

        if (spotsTotal == 0) {

            // default values

            final Random random = new Random();

            spotsTotal = random.nextInt(SLOTS_MAX_DEFAULT) + 1;
            spotsForTrucks = random.nextInt(spotsTotal + 1);

        } else if (spotsForTrucks > spotsTotal) {
            spotsForTrucks = spotsTotal;
        }

        System.out.println(String.format(Locale.US, "Spots %s, for trucks: %s", spotsTotal, spotsForTrucks));

        final Parking parking = new Parking(spotsTotal);
        parking.assignFirstSpots(new Truck(), spotsForTrucks);

        parkFullCars(parking);

    }

    private static void parkFullCars(final Parking pParking) {

        final int full = pParking.getSpotsCount();

        for (int i = 0; i < full; i++) {
            System.out.println(pParking.park(new Car()));
        }

    }

}
