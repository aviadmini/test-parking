package com.aviadmini.parking;

import org.junit.Assert;
import org.junit.Before;

public class ParkingTest {

    private static final int TRUCK_SPOTS = 4;
    private Parking mParking;

    @Before
    public void setUp()
            throws Exception {

        this.mParking = new Parking(10);
        this.mParking.assignFirstSpots(new Truck(), TRUCK_SPOTS);

    }

    @org.junit.Test
    public void park()
            throws Exception {

        int parked = 0;
        int notParked = 0;
        for (int i = 0; i < 6; i++) {

            if (this.mParking.park(new Truck()) >= 0) {
                parked++;
            } else {
                notParked++;
            }

        }

        Assert.assertEquals(parked, 4);
        Assert.assertEquals(notParked, 2);

        for (int i = 0; i < 10; i++) {

            if (this.mParking.park(new Car()) >= 0) {
                parked++;
            } else {
                notParked++;
            }

        }

        Assert.assertEquals(parked, 10);
        Assert.assertEquals(notParked, 6);

    }

    @org.junit.Test
    public void parkAndUnpark()
            throws Exception {

        final Car car = new Car();

        Assert.assertEquals(this.mParking.park(car), 0);
        Assert.assertEquals(this.mParking.unpark(0), car);

    }

}